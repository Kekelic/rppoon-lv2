﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPPOON_LV2
{
    class Program
    {
        static void Main(string[] args)
        {
            DiceRoller diceRoller = new DiceRoller();
            int numberOfSides = 6;
            int numberOfDice = 20;

            for(int i = 0; i < numberOfDice; i++)
            {
                Die die = new Die(numberOfSides);
                diceRoller.INsertDie(die);
            }

            diceRoller.RollAllDice();

            IList<int> rollingResults = diceRoller.GetRollingResults();
            foreach(int rollingResult in rollingResults)
            {
                Console.WriteLine(rollingResult);
            }
        }
    }
}
